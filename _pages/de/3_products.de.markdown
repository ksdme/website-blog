---
layout: cta-page
title: Produkte
permalink: /products/
lang: de
---

{% include products_de.html %}

## Wie wir mit Dir arbeiten

{% for proc in site.data.process_de %}
<div id="flexbox" style="display: flex; align-items: center">
  <div>
    <img src="/assets/img/home/icon_{{ proc.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
  </div>
  <div style="padding: 0.75em">
    <h3>{{ proc.heading }}</h3>
    <p>{{ proc.text }}</p>
  </div>
</div>
{% endfor %}
