---
layout: cta-page
title: Products
permalink: /products/
lang: en
---

{% include products_en.html %}

## How we work with you

{% for proc in site.data.process_en %}
<div id="flexbox" style="display: flex; align-items: center">
  <div>
    <img src="/assets/img/home/icon_{{ proc.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
  </div>
  <div style="padding: 0.75em">
    <h3>{{ proc.heading }}</h3>
    <p>{{ proc.text }}</p>
  </div>
</div>
{% endfor %}
