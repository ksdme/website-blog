---
layout: post
title:  "The Number One Reason Why Startups Fail"
date:   2018-04-04 03:52:21 -0500
author: "Whitney Tennant"
profile_link: "https://www.linkedin.com/in/whitney-tennant-11a691151/"
categories: Startup
lang: en
image:
  feature: rocket-downwards.png
  credit: "Photoroyalty — Freepik.com"
---

You’re getting started on your new project. It could be a startup, or a groundbreaking new application allowing you to conquer a new market. You’re all excited. You want to get started. You’re ready to spend the money to make it happen. Everything is set for your next big thing.
### So what happens next?

After working on many such projects, we have learned the next step is the most difficult:

The number one reason why startups and new products fail is and always has been [missing the market need](https://www.forbes.com/sites/groupthink/2016/03/02/top-20-reasons-why-startups-fail-infographic/).

In the excitement of starting a business, it’s difficult to start incrementally and test the market every step of the way. Keeping that in mind is crucial and the solution is to test your idea fast before you invest money in unnecessary elements.

One of the quickest ways to assess a certain idea and test assumptions is to create a **“Minimum Viable Product” (MVP)**. An MVP is any “setup” that covers only the absolutely necessary features to solve your core problem.

For example, let’s assume I’m inventing the first online shop ever. The problem I’m solving is people spending way too much time walking around outside stores to get products they like. So I am imagining a website where they can order items without leaving their house.

This is a completely new idea. There are no competitors left to study and I need to try out my idea, see it working.

An MVP for this only covers the **absolutely needed features**. It could be a static website where I list the products that I believe my customers want to buy and under each and every one of them I place a link to email me. Users would simply email me, I would arrange payment with them individually, go to the store, buy the product for them and send it over.

With this setup I can test my idea with a minimal investment. If users are ignoring the concept the world may not be ready for online shopping yet. If they’re picking it up, I can get my app going and I can tell potential investors how this has a definite potential to **being profitable** — also I’m already able to adjust my product development based on my users priorities as I’m constantly in touch with them.

It is crucial to test your ideas. Usually a startup is addressing a gap in the market, however users might only see the value in some of the aspects. The best solution is to provide lightweight features which you can expand on once they have been validated.

The number one reason for startup failure can be addressed. **Avoid failure and test early**.

When you start out usually there is no money to waste on what won’t get used. Rooting in the essentials and refining value core propositions has proved to be the best way to prevent failure in our experience after working with many startups

We are always happy to hear from you, let us know what you think and visit us at [viperdev.io](https://viperdev.io/) or have a look [here](https://viperdev.io/startup/2017/12/14/the-beginning-of-an-mvp).
