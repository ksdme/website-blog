---
layout: post
title:  "Turn Your Business Idea into an App"
date:   2018-05-03 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: en
image:
  feature: smartphone_dock.png
  credit: "Freepik.com"
---

Are you new to the app world? While having an optimized website is a great start, an app is the best way to get your product in front of users, especially since more than 50 percent of global web traffic originates from mobile devices. Here’s everything you need to know before building an application for your business.

### Identify the problem or need
A business is going to be successful if it offers a **solution to a problem** or meets the needs of consumers, at an **affordable price**. Start by identifying what people need, not by brainstorming ideas for businesses that could become the next big thing. You have undoubtedly encountered frustrating products or services and wished someone would come up with a better solution. Adam Goldstein, the co-founder of Hipmunk, said it was his awful experiences booking flights for the MIT debate team that motivated him to start his business. He thought there had to be a better way to search for flights online, so he found one.

### Find the unique selling points of your business
Don’t focus so much on the competition, but rather on **what makes your business stand out** in the eyes of your potential customers. Understanding your audience and highlighting the **unique benefits of your product** or service will help you create a product that your users will really want to use and recommend. As Seth Godin stated, 
>*Instead of working so hard to prove the skeptics wrong, it makes a lot more sense to delight the true believers. They deserve it, after all, and they’re the ones that are going to spread the word for you.* 

### Apply the lean startup methodology
Validating your idea before investing time and money is essential. The lean startup methodology aims to help entrepreneurs **use their resources in the most efficient way**, and encourages experimentation over elaborate planning. Replacing intuition with customer feedback will help you get a clear vision of how your application will be used and what aspects should be improved. Based on the **users feedback**, you can start building a full-featured product right after eliminating the risk of wasting resources.

### Hire a team that shares your vision
In order to accomplish your goal of creating a successful application, take into consideration that this often requires expertise in a range of skills. Look for a developer **who is invested in your idea**, someone who shares your vision, this will be an important factor in the creation of your product. In terms of hybrid apps your focus should be on finding a developer that offers you an **appropriate budget**, instead of someone who will offer java apps for high development prices. Especially in the beginning, investing into native apps can incur an unbearable cost. For an initial version, **a hybrid approach (like Ionic) will save 40-80% of the cost** when compared to building several dedicated native apps.

To summarise, if you decide to turn your business idea into an app, make sure to:
* Identify the problem or need
* Find the unique selling points of your business
* Apply the lean startup methodology
* Get a team that shares your vision

You might also be interested in reading about [The Right Way to Define Features for an MVP](https://viperdev.io/startup/2018/04/25/the-right-way-to-define-features-for-an-mvp). 
Want to turn your idea into a business? [Book a call](https://calendly.com/viper) with us.